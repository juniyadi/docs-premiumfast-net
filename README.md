# Pengenalan

## Apa itu PFN Docs?

PFN Docs atau Premium Fast Network Documentations merupakan situs tutorial dasar dalam penggunaan setiap produk pada [https://www.premiumfast.net](https://www.premiumfast.net)

{% hint style="info" %}
Jika ada kendala, jangan ragu untuk menghubungi kami di :

* [cs@premiumfast.net ](mailto:cs@premiumfast.net)
* [Tiket Bantuan](https://manage.premiumfast.net/contact.php)
{% endhint %}

## Cara Menggunakan PFN Docs

Jika anda pertama kali menggunakan website ini, silahkan pelajari dasar penggunaan dahulu agar memudahkan dalam pembacaan tutorial.

Dalam PFN Docs terbagi menjadi 3, yaitu : 

1. Menu

   Semua index tutorial dari semua produk kami

2. Kontent

   Isi dari kontent yang anda pilih dari `Menu`

3. Index Kontent

   Index dari kontent pada nomor 2 \(Hanya untuk tutorial yang panjang\)

![](.gitbook/assets/selection_059.png)

{% hint style="info" %}
Anda juga bisa menggunakan fitur **Search** pada pojok kanan atas.
{% endhint %}

