# Cara Melakukan Pembayaran Melalui Alfamart/Pegadaian/Kantor POS

### Pada Invoice, Pilih Metode Pembayaran: Alfamart/Pegadaian/Kantor Pos dan Klik PAY NOW

Kemudian akan muncul tampilan seperti dibawah ini, **SALIN** dan **SIMPAN KODE PEMBAYARAN** anda. Kemudian ikuti instruksi **CARA PEMBAYARAN** sesuai Pojok Kiri Bawah.

![](../.gitbook/assets/191114_0005.png)

### Pembayaran Melalui Alfamart

1. Datang langsung ke kios Alfamart
2. Informasikan kepada kasir bahwa anda akan melakukan **"Pembayaran Telkom"**
3. Kasir akan meminta kode pembayaran. Berikan catatan yang sudah anda buat atau bacakan kode tersebut
4. Selanjutnya kasir akan memasukkan kode pembayaran yang anda berikan kedalam sistem dan akan memberitahukan nominal yang harus anda bayar
5. Lakukan pembayaran sesuai jumlah yang diinformasikan dan tunggu hingga proses selesai.

### Pembayaran melalui Pegadaian

1. Datang langsung ke cabang Pegadaian terdekat
2. Informasikan kepada teller bahwa anda akan melakukan **"Pembayaran Telkom"**
3. Teller akan meminta kode pembayaran. Berikan catatan yang sudah anda buat atau bacakan kode tersebut
4. Selanjutnya teller akan memasukkan kode pembayaran yang anda berikan kedalam sistem dan akan memberitahukan nominal yang harus anda bayar
5. Lakukan pembayaran sesuai jumlah yang diinformasikan dan tunggu hingga proses selesai.

### Pembayaran melalui Kantor Pos

1. Datang langsung ke kantor Pos terdekat
2. Informasikan kepada petugas bahwa anda akan melakukan **"Pembayaran Telkom"**
3. Petugas akan meminta kode pembayaran. Berikan catatan yang sudah anda buat atau bacakan kode tersebut
4. Selanjutnya petugas akan memasukkan kode pembayaran yang anda berikan kedalam sistem dan akan memberitahukan nominal yang harus anda bayar
5. Lakukan pembayaran sesuai jumlah yang diinformasikan dan tunggu

### 

