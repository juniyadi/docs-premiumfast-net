# Pre-Sales Question

## Bagaimana cara mendaftar di Website PFN?

Anda bisa mendaftar langsung melalui link berikut : [https://manage.premiumfast.net/register.php](https://manage.premiumfast.net/register.php)

## Bagaimana cara order produk di PFN?

Anda bisa langsung klik `BUY NOW` di produk yang ingin anda beli pada website utama kami [https://www.premiumfast.net](https://www.premiumfast.net)

## Kapan saya harus transfer pembelian baru atau perpanjang?

Anda bisa transfer kapan saja 24 Jam non-stop selama invoice anda sudah terbit.

## Apakah produk di PFN bisa diperpanjang tanpa rubah ini itu?

Tentu, 14 hari sebelum jatuh tempo, sistem kami akan secara automatis mengirimkan detail tagihan perpanjang produk anda.

Anda bisa langsung membayar tagihan tersebut hingga maksimal H+1 setelah jatuh tempo, jika lebih dari itu, produk akan disuspend/terminated.

## Bagaimana cara mengecek tagihan saya?

* Login ke [Client Area](https://manage.premiumfast.net)
* Klik pada menu `TAGIHAN` 
* Kemudian `TAGIHAN SAYA`

## Bagaimana cara membayar tagihan saya?

Anda hanya perlu mengikuti instruksi sesuai tagihan invoice.

## Saya sudah transfer? Apakah pembayaran saya sudah diterima/konfirmasi?

Saat ini di PFN memiliki 2 metode transfer:

1. Automatis
2. Manual

Jika anda transfer dengan pembayaran automatis maka anda tidak perlu melakukan konfirmasi, karena system akan konfirmasi secara automatis.

Jika anda transfer manual ke rekening kami, silahkan konfirmasi melalui website [https://pay.premiumfast.net/](https://pay.premiumfast.net/)

## Berapa lama produk saya diaktifkan?

Beberapa produk kami akan aktif automatis jika dana sudah diterima / invoice tagihan sudah lunas, seperti :

1. Domains
2. Shared Hosting
3. Leechbox
4. RDP
5. KVM VPS
6. VPN

Produk yang diaktifkan secara manual :

1. Seedbox
2. Dedicated Server Windows

