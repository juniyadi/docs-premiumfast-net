# Cara Cek Kestabilan Koneksi

Untuk mengetahui apakah koneksi anda saat ini lancar atau tidak, terkadang tidak hanya perlu mengecek dengan [Speedtest](https://www.speedtest.net/), hal ini karena website atau server yang anda akses tentu berbeda dengan yang ada pada Speedtest tersebut.

Ada 2 cara untuk mengecek kestabilan server, yaitu **PING** dan **TRACE ROUTE.**

{% hint style="warning" %}
Umumnya pengecekan ini melihat ms \(milisecond\) dan hops.  
  
Semakin kecil ms maka semakin dekat, begitu pula hops, semakin sedikit maka paket anda tidak di puter-puter ke tempat lain.

* **1 - 50ms = Sangat Cepat**
* **50 - 150ms = Cepat**
* **151 - 250ms = Sedang/Mulai Lambat**
* **251 - 350ms = Lambat**
* **lebih dari 350ms = Koneksi Buruk/Parah**

**Beberapa website atau server tertentu juga kadang walaupun ping diatara 100-250ms, tetapi sangat lambat, ini bisa juga terjadi karena masalah dikoneksi anda, jika seperti ini, anda harus menghubungi pihak penyedia koneksi internet yang anda gunakan, karena hanya merekalah yang bisa melakukan perbaikan.**
{% endhint %}

### **PING**

PING _\(Packet Internet Gopher\),_ adalah sebuah aplikasi untuk pengecekan protokol koneksi jaringan, ping tersedia dari berbagai semua os yang ada dipasaran seperti windows, linux ataupun mac.

* Untuk menggunakan PING, anda bisa melakukan di CMD \(Windows\) atau Terminal  \(Linux atau MAC\).

Gunakan perintah **`ping ip_server`**

![Test Ping dengan Windows \(CMD\)](../.gitbook/assets/191011_0003.png)

{% hint style="success" %}
**`8.8.8.8 Merupakan IP_Server dan DNS Google, jika anda ingin cek ke situs tertentu, atau server tertentu, maka gantilah IP_Server 8.8.8.8 sesuai dengan alamat ip atau website yang ingin anda test.`**
{% endhint %}

### TRACE ROUTE

TRACE ROUTE digunakan untuk melacak, ka arah mana jalur paket yang dilewati untuk mencapai sebuah website atau server.

Untuk menggunakan Trace Route anda bisa menjalankan command :

* Windows/Mac:  **`tracert ip_server`**
* Linux: **`traceroute ip_server`**

![Test Trace Route dengan Windows \(CMD\)](../.gitbook/assets/191011_0004.png)

### Tips: Cek Hasil Trace Route di Google Map

Ada sebuah website yang menyediakan pengecekan trace route dan langsung dibuat map, walaupun tidak sepenuhnya akurat 100%, tetapi ini sudah sangat membantu.

> URL Website [https://stefansundin.github.io/traceroute-mapper/](https://stefansundin.github.io/traceroute-mapper/)

#### Cara Copy Paste Data di CMD

Jika anda menggunakan CMD pada windows, anda bisa mengikuti cara berikut untuk mengambil data pada cmd \(copy-paste\).

* Klik kanan pada atas CMD kemudian klik menu `Edit` =&gt; `Select All`

![](../.gitbook/assets/191011_0005.png)

* Kemudian sama seperti sebelumnya, klik menu `Edit` =&gt; `Copy`

![](../.gitbook/assets/191011_0006.png)

* Anda bisa langsung `CTRL +V` di notepad atau lainnya

#### Cara Menggunakan Traceroute Mapper

* Akses website [https://stefansundin.github.io/traceroute-mapper/](https://stefansundin.github.io/traceroute-mapper/)
* Ubah data dari CMD tadi, seperti ini 

![](../.gitbook/assets/191011_0007.png)

* Klik **Map It!**

![](../.gitbook/assets/191011_0008.png)

* Muncul kemana paket anda dikirimkan ketika mengunjungi sebuah website.

