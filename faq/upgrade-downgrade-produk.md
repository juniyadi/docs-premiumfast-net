# Upgrade / Downgrade Produk

## Apakah saya bisa melakukan upgrade produk XXX?

Jika anda ingin melakukan upgrade, silahkan cek pada menu `Layanan` =&gt; `Layanan Saya` kemudian klik pada produk yang ingin di upgrade, pada menu sebelah kiri, ada menu `Upgrade`.

Proses upgrade akan dilakukan jika invoice tagihan upgrade telah lunas.

{% hint style="warning" %}
Jika produk tidak bisa di upgrade, maka menu tersebut tidak akan muncul.
{% endhint %}

## Berapa lama waktu proses upgrade?

Tergantung produk yang anda beli, bisa instant atau manual.

## Apakah saya bisa downgrade KVM VPS?

Tidak, untuk downgrade ada pengurangan penyimanan \(disk\), hal ini biasanya menyebabkan error. Kami sarankan untuk cancel produk saat ini, dan melakukan order baru pada KVM VPS dengan paket yang lebih kecil sesuai kebutuhan anda.

## Invoice upgrade telah lunas, tetapi statusnya masih sama tidak ada berubah.

Silahkan hubungi kami di [Tiket Support](https://manage.premiumfast.net/submitticket.php?step=2&deptid=2) agar dilakukan pengecekan hal tersebut.

