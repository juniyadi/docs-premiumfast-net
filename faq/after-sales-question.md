# After-Sales Question

## Saya sudah transfer, tetapi invoice masih UNPAID?

Jika anda transfer manual kepada kami, pastikan untuk mengkonfirmasi ke : [https://pay.premiumfast.net/](https://pay.premiumfast.net/)

Jika anda melakukan transfer automatis, silahkan hubungi kami di [Tiket Support](https://manage.premiumfast.net/submitticket.php?step=2&deptid=2)

## Invoice saya sudah lunas, tetapi waktu expired tidak perpanjang.

Silahkan menghubungi kami melalui [Tiket Support](https://manage.premiumfast.net/submitticket.php?step=2&deptid=2) agar kami cek.

## Saya mempunyai banyak produk dan tagihan tiap bulan, bisakan PFN perpanjang automatis semua produk saya?

Tentu, kami memiliki system dimana semua tagihan akan automatis lunas/terbayar jika anda memiliki credit atau saldo pada akun anda, selama saldo anda mencukupi, maka ketika tagihan keluar, automatis memotong saldo tersebut.

## Saya sudah deposit saldo/credit, tetapi invoice saya masih tetap UNPAID.

System kami hanya akan memotong saldo/credit secara automatis ketika invoice pertama kali terbit, jika invoice anda sudah terbit kemudian anda melakukan deposit, maka anda harus melakukan secara manual untuk memasukkan credit/saldo ke invoice tagihan yang sudah terbit tersebut.

Untuk melakukannya, anda hanya perlu membuka tagihan invoice anda, kemudian klik `Apply Credit`

![Halaman Tagihan Invoice](../.gitbook/assets/selection_060.png)

## Bisakan saldo/credit saya di transfer/tarik ke rekening pribadi \(Withdraw\) ?

Tidak, semua saldo/credit yang di depositkan hanya berlaku untuk pembelian dan pembayaran pada PFN. Tidak bisa ditarik ke rekening pribadi.

## Saya ingin berhenti berlangganan produk, bagaimana caranya?

Anda bisa mengabaikan invoice tagihan yang telah terbit, sistem kami akan automatis cancel jika invoice tersebut tidak dibayar setelah jatuh tempo.

Atau anda bisa melakukan `Cancel` produk dari halaman `Layanan` =&gt; `Layanan Saya` kemudian klik pada produk yang ingin anda batalkan, pada menu kiri, klik menu `Minta Pembatalan`

Maka sistem kami tidak akan mengirim tagihan invoice ,atau jika invoice  telah terbit, invoice akan dibatalkan, dan produk akan dihapus ketika expired.

## Saya telah melakukan request pembatalan layanan, tetapi saya ingin memperpanjang produk itu kembali, apakah bisa?

Tentu, silahkan hubungi kami di [Tiket Support](https://manage.premiumfast.net/submitticket.php?step=2&deptid=2)

## Saya telah melakukan request pembatalan layanan, tetapi saya ingin memperpanjang produk itu kembali, apakah bisa?

Tentu, silahkan hubungi kami di [Tiket Support](https://manage.premiumfast.net/submitticket.php?step=2&deptid=2)

