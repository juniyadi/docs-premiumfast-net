# Ketentuan Layanan \(Terms of Services\)

{% hint style="warning" %}
Harap membaca Ketentuan Layanan pada halaman ini sebelum anda membeli, menggunakan, dan berlangganan semua produk pada PREMIUM FAST NETWORK, saat anda melakukan order pada website kami \(https://premiumfast.net\) berarti anda setuju dengan ketentuan layanan yang terdapat pada semua produk kami. Anda secara sadar dan sehat baik secara jasmani maupun rohani saat melakukan order layanan kami.
{% endhint %}

## **1.Pembelian Baru**

Silahkan mengisi form order dan memasukan data secara lengkap dan benar, KAMI BERHAK MENGHAPUS AKUN ANDA, MEMBATALKAN ORDER ANDA jika kami menemukan anda menggunakan data PALSU yang digunakan saat mengisi form data pada order kami

## **2. Billing**

Pelanggan diwajibkan membayar atau melunasi pembayaran sebelum jatuh tempo.  
Billing Sistem kami akan memberikan notifikasi pembayaran dalam beberapa tahap :

1. 14 hari sebelum jatuh tempo, pelanggan akan diberikan notifikasi dalam bentuk email dan sms langsung ke hp pelanggan.
2. 7 hari sebelum jatuh tempo, pelanggan akan diingatkan tentang pembayaran yang belum dilunasi, pelanggan akan diberikan notifikasi dalam bentuk email dan sms langsung ke hp pelanggan beserta tanggal jatuh tempo
3. 3 hari setelah jatuh tempo, pelanggan akan diingatkan tentang pembayaran yang belum dilunasi, pelanggan akan diberikan notifikasi dalam bentuk email dan sms langsung ke hp pelanggan beserta tanggal jatuh tempo

Jika dalam waktu 1×12 jam setelah jatuh tempo ternyata pihak kami belum menerima pembayaran maka produk akan kami terminated \(HAPUS\) secara permanent. Harap melunasi pembayaran anda sebelum jatuh tempo.

## **3. Pembayaran**

PREMIUM FAST NETWORK \(PFN\) Menerima pembayaran melalui beberapa metode yang saat ini kami dukung seperti :

INDONESIA USER

* Credit Card/Debit Card \(Indonesia\)
* BCA KlikPay
* Virtual Account 
  * ATM Bersama
  * BNI
  * BRI
  * CIMB
  * MANDIRI
  * MAYBANK
  * PERMATA
* Ritail 
  * Alfamart
  * Kantor Pos
  * Pegadaian
* OVO
* BANK CENTRAL ASIA \(BCA\)
* BANK NEGARA INDONESIA \(BNI\)
* BANK RAKYAT INDONESIA \(BRI\)
* BANK MANDIRI
* BANK BTPN \(JENIUS\)
* BANK PERMATA SYARIAH

INTERNATIONAL USER \(Saat ini sedang tidak menerima pembayaran untuk international user\)

## **4. Pengembalian Dana**

Semua produk pada PREMIUM FAST NETWORK menerima ketentuan PENGEMBALIAN DANA dengan catatan anda dapat menjelaskan kenapa anda melakukan PENGEMBALIAN DANA, pada beberapa kasus jika kami tidak menerima pernyataan yang sesuai dengan kondisi anda tidak dapat melakukan PENGEMBALIAN DANA.  
  
PENGEMBALIAN DANA dapat dilakukan saat 12 JAM setelah produk anda aktif. Melebihi waktu tersebut dan anda tidak dapat melakukan permintaan PENGEMBALIAN DANA.Semua permintaan tanpa alasan, PENGEMBALIAN DANA tidak dapat dilakukan.Proses PENGEMBALIAN DANA dilakukan saat jam kerja \(senin – jum’at\).  
  
**PENGEMBALIAN DANA dapat dilakukan hanya pada produk LEECHBOX & SEEDBOX, selain produk tersebut kami tidak menyediakan layanan PENGEMBALIAN DANA.**

## **5. Support**

Kami memberikan support kepada seluruh pelanggan selama 24x7x365 yang berarti anda dapat langsung membuat tiket support kepada kami \(status tiket OPEN\) baik itu pagi, siang ataupun malam, dan harap menunggu hingga beberapa menit hingga beberapa jam, tim support kami akan langsung membalas tiket anda, harap dicatat penyelesaian masalah dalam beberapa produk bisa ditangani dalam waktu beberapa menit \(tergantung dari masalah yang dihadapi\), jika dalam waktu beberapa jam kami belum membalas, harap bersabar hingga staff kami online dan permasalahan anda terjawab \(status tiket ANSWARE\).

## **6. Produk Support**

Misalnya pada RDP, terdapat “Support BOT” yang berarti, yang berarti, anda dapat menggunakan rdp tersebut untuk BOT, semua software yang berhubungan dengan hal bot tersebut TIDAK disediakan oleh Premium Fast Network, anda harus mengerti cara mengconfigurasi, cara menggunakan aplikasi BOT sehingga dapat berjalan dengan lancar pada RDP anda, jika anda membutuhkan installasi software BOT anda dapat membuka tiket ke support department kami dan akan dibantu HANYA installasi saja \(TIDAK DENGAN CONFIGURASI\).  
  
Kami tidak menyediakan jasa mengani pengecekan website pada hosting anda, yang kami support hanya hardware dan network, jika ada masalah pada settingan konfigurasi dsb, akan kami bantu sebisanya, akan tetapi jika sudah terlalu dalam masalah tersebut, kami sarankan untuk menyewa jasa profesional mengenai masalah yang anda alami.  
  
Harap dicatat saat anda order produk kami sesuaikan dengan kebutuhan anda, penggunaan yang melanggar syarat dan ketentuan yang telah kami atur dapat menyebabkan akun anda di suspend tanpa mendapatkan refund.

## **7. Upgrade & Downgrade Produk**

Saat ini kami tidak menyediakan UPGRADE ataupun DOWNGRADE untuk PRODUK :

1. Semua Produk : RDP
2. Semua Produk : Dedicated Server Windows
3. VPN

Produk yang bisa di Upgrade Saat ini hanya :

1. Semua Produk : Leechbox
2. Semua Produk : Seedbox
3. Semua Produk : Shared Hosting
4. Semua Produk : KVM VPS

Saat anda melakukan order, anda secara sadar telah memahami produk apa yang anda beli.

## **8. Backup**

Untuk semua produk kami, kami tidak menyediakan fitur backup. Pelanggan bertanggung jawab secara penuh atas data-data pada produk yang dibeli, baik itu Leechbox/Seedbox/VPS/RDP. Harap membackup secara berkala, data pada produk anda masing-masing. Jika terjadi error pada harddisk yang menyebabkan data hilang/rusak maka bukan tanggung jawab kami akan data tersebut.  
  
Khusus Shared Hosting kami melakukan backup mingguan.

## **9. Ketentuan Penggunaan Layanan Leechbox/Seedbox**

Dalam pembelian produk leechbox/seedbox anda dilarang melakukan hal-hal seperti berikut :

1. Share akun kepada orang lain \(forum/grup/blog/website\)
2. Multi-Login \(Login secara bersamaan dalam satu waktu berbeda ip\)
3. Share file dari server kami secara direct \(forum/grup/blog/website\)

{% hint style="danger" %}
Khusus SEEDBOX, anda dilarang menggunakan seedbox untuk seeding file-file dari public torrent \(KickASS, Piratebay, Extratorrent dan website-website public torrent lainnya\)

  
Jika staff kami menemukan sesuatu yang mencurigakan, maka kami akan SUSPEND AKUN ANDA SECARA PERMANENT TANPA PENGEMBALIAN DANA.
{% endhint %}

{% hint style="info" %}
Note:  
Jika kami menerima laporan DMCA kepada server kami \(yang menyebabkan server di suspend\), kami berhak menghapus file tersebut baik dengan notifikasi ataupun tidak.

  
Kami berusaha untuk tidak terkena DMCA pada server leechbox/seedbox kami.  
pelaporan dmca dapat ditujukan kepada email : report-abuse@premiumfast.net
{% endhint %}

## **10. Ketentuan Penggunaan Layanan Shared Hosting**

Kami melarang pengguna kami melakukan kegiatan-kegiatan ilegal pada shared hosting kami seperti daftar dibawah ini :

1. Seluruh Pelanggan Dilarang menggunakan hosting untuk kegiatan DDOS Attack, Port Scanning atau kegiatan ilegal yang merugikan lainnya.
2. Seluruh Pelanggan Dilarang menggunakan autoblog, dan plugin sejenis autoblog.
3. Seluruh Pelanggan Dilarang menggunakan sebagai tempat host site pornografi, sara, atau sejenisnya.
4. Seluruh Pelanggan Dilarang menggunakan untuk IRC, BOTNET dan program proxy lainnya.
5. Seluruh Pelanggan Dilarang menggunakan untuk rapidleech dan program sejenis lainnya.
6. Seluruh Pelanggan Dilarang menggunakan untuk file hosting dan image hosting.
7. Seluruh Pelanggan Dilarang mengupload file bervirus / pendukung hacking.
8. Seluruh Pelanggan Dilarang menggunakan hosting sebagai tempat phising, hacking, carding atau sejenisnya.
9. Seluruh Pelanggan Dilarang menggunakan cronjob berlebihan, dan dengan waktu dibawah 5 menit per 1 cronjobnya
10. Seluruh Pelanggan Dilarang menggunakan untuk keperluan Twitter, Facebook, Instagram Client atau program Auto Like/Follow/Post/Comment atau program bot sosial media lainnya.
11. Seluruh Pelanggan Dilarang menggunakan untuk website untuk forum dan forum berbau hacking
12. Seluruh Pelanggan Dilarang menggunakan untuk mail spamming/spam-spam sejeninsya.

**Jika kami menemukan hosting anda melanggar ketentuan diatas, maka produk anda kami kami suspend 1x dan peringatan, jika 3x produk anda disuspend karena melanggar tos diatas, maka suspend ke-4 adalah yang terakhir dan produk anda akan kami terminated \(TANPA BACKUP\)**

## **11. Ketentuan Penggunaan Layanan RDP**

PENGGUNAAN CPU MAKSIMAL 20% DAN RAM 2 HINGGA 5 GB SELAMA 2 JAM – 6 JAM \(JIKA PENGGUNAAN ANDA DIBAWAH BATAS TERSEBUT, MAKA TIDAK MASALAH\), JIKA MELEBIHI AKUN AKAN DI BERI PERINGATAN, PERINGATAN HINGGA 3X, JIKA TERDAPAT PERINGATAN HINGGA 3X, AKUN AKAN DI SUSPEND PERMANENT.  
  
PENYIMPANAN DATA ANDA BERADA PADA “PARTISI SESUAI USERNAME ANDA”, Penyimpanan pada folder lain tidak di perbolehkan & AKAN DI HAPUS PERMANENT JIKA ADA DATA PADA folder DOWNLOADS, DOCUMENT, PICTURE, VIDEOS, DESKTOP DAN PADA PARTISI C:/.  
  
Jika kami menemukan akun anda menggunakan atau menyimpan data pada folder seperti folder DOWNLOADS, DOCUMENT, PICTURE, VIDEOS, DESKTOP DAN PADA PARTISI C:/, maka akaun anda akan mendapat PERINGATAN 1 \(pertama\) hingga SUSPEND PERMANENT.  
  
ANDA TIDAK MEMPUNYAI HAK AKSES ADMIN, JIKA MEMERLUKAN BANTUAN DALAM INSTALL APLIKASI, SILAHKAN HUBUNGI KAMI VIA TIKET KE SUPPORT DEPARTMENT.

Banned Activity :

1. Hacking Activities
2. Spamming
3. Port Scanning
4. DDOS
5. Sharing our IP publicly
6. Mining Tools
7. Virus & Malware
8. Trojan
9. Scripts Abuse
10. SEO Tools
11. Abusing Network
12. Video Encoding
13. Cpu/Ram Abusive Applications
14. Public Torrents Seeding
15. RUNNING Multiple Winrar processes
16. Running Winrar Process without Normal Mode
17. Downloading/Uploading/Seeding More Than 5-10 Torrent or any files At a time

Jika staff kami mendeteksi akun anda melakukan hal-hal diatas, maka akun rdp anda akan di Suspend Sementara atau Permanent \(TANPA REFUND\).  
  
**12. Ketentuan Penggunaan Layanan KVM VPS**  
Sebelum anda menggunakan VPS untuk file, data, website dsb, terlebih dahulu anda membaca hal berikut ini untuk kenyamanan penggunaan VPS anda.  
Pada penggunaan VPS kami melarang anda menggunakan VPS dengan tujuan untuk :

1. Denial of Service \(DoS\)/DDOS attacks
2. Hacking Activities
3. Carding Activities
4. Spamming
5. Port Scanning
6. Virus & Malware
7. Scripts Hacking/Cracking or ilegal Activities
8. Child Pornography or Exploitation
9. Proxy / VPN Public \(Private Only its allowed\)

**Special for USA VPS :**  
**We are not allowed USA VPS for used "Unauthorized or illegal software, movie, or sound recordings in violation of copyright law, including but not limited to the DMCA"**

Anda dapat menjalankan program download torrent, tetapi dengan akses pribadi bukan untuk umum pada VPS Canada/France.  
Anda dapat menggunakan cpu 100%, selama penggunaan tersebut tidak terus menerus maka tidak masalaah, akan tetapi jika penggunaan anda tinggi dan mengganggu node server, maka langkah pertama vps anda akan kami suspend dan anda akan dikirimkan tiket mengenai hal tersebut.Jika 3x anda menerima tiket suspend dalam waktu 30 hari, maka vps akan disuspend permanent tanpa refund..!!

Melanggar TOS dapat menyebabkan VPS SUSPEND PERMANENT TANPA REFUND..!!!

Info tambahan tentang RAM pada vps: 1000 MB = 1 GB \(berlaku kelipatan\) yang akan muncul pada vps windows maka tidak akan utuh terlihat 1 GB misalnya.

## **13. Ketentuan Layanan Dedicated Server Windows**

Pada penggunaan Dedicated Server Windows, kami melarang anda menggunakan dengan tujuan untuk :

1. Denial of Service \(DoS\)/DDOS attacks
2. Hacking Activities
3. Carding Activities.
4. Spamming
5. Port Scanning
6. Virus & Malware
7. Scripts Hacking/Cracking or ilegal Activities
8. Child Pornography or Exploitation
9. Proxy / VPN Public \(Private Only its allowed\)

Atau kegiatan ilegal lainnya yang melanggar UU ITE.  
Maximal Pengiriman Dedicated Server setelah order \(Invoice Lunas\) adalah 7 Hari kerja.  
Karena ada perbedaan waktu indonesia dengan france \(Eropa\), Semua dedicated yang telah diorder dan lunas, tidak bisa direfund.

Perlu dicatat bahwa pada service server anda menggunakan self-manage service, yang berarti bahwa anda mendapat dukungan untuk request pengecekan server misalnya jika ingin restart server \(hard reboot\), masalah hardware, jaringan dan reinstall server.

Kami memberikan FREE installasi pada pertama pembelian, anda juga dapat request REINSTALL server kembali secara gratis sebanyak 1x dalam 1 bulan, jika dalam 1 bulan anda request installasi lebih dari 1x, maka anda akan dikenakan biaya tambahan sebesar Rp. 250.000,-/Installasi. \(Installasi menggunakan OS Windows Server 2012 R2\).

Perlu dicatat jika anda ingin reinstall ke os windows lain, anda harus memilki lisensi windows sebelum request, akan tetapi jika anda menggunakan os windows server 2012 R2 \(FREE tanpa harus anda membeli lisensi, lisensi sudah kami aktifkan saat installasi\).

Jika anda membutuhkan settingan/setup tambahan, hal ini hanya bisa saat pertama kali install saja \(Max 1x12 Jam setelah anda menerima email ini, lebih dari 12 jam, anda akan dikenakan biaya tambahan settingan server sebesar Rp. 100.000/Request \(Tergantung Aplikasi dan Tingkat kesulitan settingan\)\).

## **14. Software Pihak Ketiga**

Pada dasarnya, semua produk kami, tidak ada dukungan dari Software Pihak Ketiga, yang berarti, jika software tersebut tidak running pada produk kami misalnya karena :

1. Kesalahan Configurasi
2. Aplikasi Expired
3. Aplikasi Tidak Dapat digunakan sebagaimana mestinya
4. dsb

hal tersebut bukan tanggung jawab kami, Premium Fast Network hanya support Hardware & Network, misalnya pada saat anda berlangganan RDP kepada kami, ternyata RDP tidak dapat login, atau password salah, hal tersebut dapat langsung menghubungi kami, akun RDP akan kami check dan diperbaiki.  
tetapi jika misalnya anda membeli RDP untuk menjalankan sebuah software \(misalnya untuk BOT\) ternyata software tersebut tidak dapat digunakan semestinya \(dikarenakan adanya banned/softbanned/banned permanent atau problem lainnya yang menyebabkan software tersebut tidak dapat berjalan sebagaimana mestinya\) dari pihak developer software, hal tersebut bukan tanggung jawab kami.  
Premium Fast Network sepenuhnya hanya menjual VPS/RDP, kami tidak menjual trik, kami tidak menggaransikan trik, kami tidak ada menjual yang lainnya.  
Saat user order semua produk kami \(VPS/RDP\), USER dengan sadar telah mengerti cara :

1. Cara Mengoprasional VPS/RDP
2. Cara Menggunakan VPS/RDP \(Installasi/Configurasi\)
3. Cara Memperbaiki Permasalahan ada VPS/RDP \(Kecuali Hardware & Network\)

Tetapi jika anda ada masalah pada software-software tertentu, kami akan bantu anda dengan standart dari kami. dan akan kami check terlebih dahulu permasalahan software tersebut.

## **15. Speed & Bandwith**

Kami tidak menjamin semua produk kami 100% FULL SPEED setiap waktu, karena jaringan bisa saja naik turun, gangguan dan hal teknis lainnya yang dimana saat user mengetest speed dalam mengalami lambatnya kecepatan produk tersebut.  
Untuk speedtest kami tidak terlalu menyarankan menggunakan speedtest.net sebagai acuan utama.  
Download file dari dan menuju server adalah REAL SPEEDTEST yang sering kami gunakan.  
Jika anda merasa VPS/RDP anda lemot speedtestnya, kami sarankan menggunakan download file untuk mengetes kecepatan download server dan mengupload file ke hosting seperti uptobox,uploaded,rapidgator sebagai acuan speedupload.  
Kami berkomitmen menyediakan server yang cepat untuk kebutuhan anda.

## **16. Mining Tools**

Per-tanggal 24 Juli 2017, kami tidak memperbolehkan semua produk kami \(Kecuali Dedicated Server Windows\), dilakukan untuk mining. Jika anda melakukan mining maka produk anda kami suspend tanpa refund.

{% hint style="warning" %}
Kesalahan order produk dikarenakan kelalaian user/pelanggan dalam memahami produk yang kami jual bukanlah tanggung jawab kami, ****produk tidak dapat diganti atau ditukar atau upgrade karena kesalaham pembelian.Saat anda order anda sudah paham dengan baik dan benar tentang produk tersebut. kami tersedia secara online chat, tiket maupun whatsapp jika anda bingung dengan produk tersebut.
{% endhint %}

{% hint style="danger" %}
Kami berhak melakukan perubahan, edit, penghapusan “Ketentuan Layanan” kami tanpa pemberitahuan terlebih dahulu kepada user  
  
_Update: 4 Desember 2017_
{% endhint %}

