# Cara Penggunaan WSGateway

## Login ke WSGateway?

Ketika anda telah melakukan pembayaran, maka kami akan mengirim detail login, api, dsb melalui tiket. Detail tiket bisa anda cek pada [https://manage.premiumfast.net](https://manage.premiumfast.net) pada menu `SUPPORT` =&gt; `Daftar Tiket Bantuan`

Detail juga kami kirimkan melalui email dengan detail tiket yang sama.

![1.0 - Halaman Depan / Dashboard WSGateway](../../.gitbook/assets/wsgateway-halaman-depan.png)

## Mengirim Pesan Melalui WSGateway ?

Pada gambar 1.0 sebelumnya, terlihat bahwa ada kotak `Send Message`, anda hanya perlu memasukkan nomor anda pada form isian tersebut, kemudian anda bisa langsung mengirim pesan ke nomor tujuan yang dimasukkan.

## Mengirim Pesan Massal Dengan 1x Klik ?

* Masuk ke menu `Contact Group` terlebih dahulu, buat grup contact baru.

![1.1 - List Contact Group](../../.gitbook/assets/wsgateway-contact-group.png)

* Masuk ke menu `Contact`

![1.2 - List Contact](../../.gitbook/assets/wsgateway-contact.png)

* Kemudian tambahkan contact dengan menu `Import Contact`

![1.3 - Import Contact](../../.gitbook/assets/wsgateway-contact-import.png)

{% hint style="success" %}
Masukkan format dengan pemisahan koma, contoh:

nama pertama,62857xxxxxxxxx  
nama kedua,0857xxxxxxxx  
  
Jangan gunakan format + pada awalan nomor, seperti :  
nama pertama,+62857xxxx _**&lt;= ini format salah dan pesan tidak akan terkirim**_
{% endhint %}

{% hint style="warning" %}
Semakin banyak kontak yang anda tambahkan, mungkin akan membutuhkan waktu lebih lama.
{% endhint %}

* Untuk mengirim pesan secara massal dengan 1x klik, kembali ke menu `Contact Group` dan klik tombol kuning pada action tab \(Lihat Panah Pada Gambar\)

![1.4 - Kirim Pesan Ke Group](../../.gitbook/assets/wsgateway-contact-group-send.png)

* Pilih Device dan kemudian masukkan pesan anda

![1.5 - Kirim Pesan Massal ke Group Contact](../../.gitbook/assets/wsgateway-contact-group-send-bulk.png)



## Mengecek Pesan Masuk/Keluar?

Anda bisa langsung menuju ke menu : `Message Inbox` untuk pesan masuk, dan `Message Outbox` untuk pesan keluar

## Mengecek Status Device Saya ?

Silahkan ke menu `Device` .. kemudian pada halaman utama tersebut, anda bisa cek status, type dan action.

![1.6 - Device Status](../../.gitbook/assets/wsgateway-device.png)

{% hint style="success" %}
Secara default, system kami akan melakukan reconnect jika device tiba-tiba disconnect, akan tetapi, anda bisa melakukan untuk reconnect secara manual dengan klik tombol kuning pada tab action.

System akan membutuhkan waktu setidaknya 15 menit untuk terhubung kembali secara automatis.

Jika type device anda shared, fitur action restart tidak akan berfungsi da menampilkan error.
{% endhint %}

## Setting Bot AutoReply

{% hint style="success" %}
Bot AutoReply adalah fitur yang hanya bisa digunakan pada paket private, karena dengan paket private, anda bisa menerima pesan masuk. sehingga ketika pesan masuk, system kami akan mengecek data pada bot autoreply ini.
{% endhint %}

![1.7 - List Bot](../../.gitbook/assets/wsgateway-bot-reply.png)

* Pada gambar 1.7 terlihat settingan bot yang bisa digunakan, untuk setting bot, klik add new data dan isi form seperti berikut :
  * Device = Device Anda
  * Nama = Nama Bot / Pesan
  * Short Code = Kode ini akan digunakan untuk memfilter pesan yang masuk
  * Reply Message = Balasan pessan anda
  * Type : 
    * Default = jika anda membuat default, maka semua pesan masuk akan dibalas dengan bot dengan type defaut
    * Reply Refference By Short Code \(refcode\) = jika anda membuat dengan tipe ini, maka setiap pesan masuk akan difilter berdasarkan kode pada _**`Short Code`**_

{% hint style="info" %}
Misalnya ketika anda memasukkan **hello**, maka jika ada pesan yang mengandung kata **hello** _\(Contoh: hello saya pfn\)_**,**  maka system akan membalas automatis pesan sesuai dengan yang di isi pada **reply message**

Anda bisa menggunakan multiple Short Code dan memisahkan dengan koma, contoh : **transfer,konfirmasi,pembayaran,lunas**

Maka jika ada pesan mengandung unsur multiple shortcode tersebut akan terbalas automatis
{% endhint %}

* 
![1.8 - Bot Message Setup](../../.gitbook/assets/wsgateway-bot-reply-setup.png)

## Membuat Delay Pesan Ketika Mengirim Pesan Massal

* Pada halaman `Devices`, Klik Tombol `Edit` :

![1.9 - Edit Button Device](../../.gitbook/assets/191127_0001.png)

* Akan muncul pop up detail setting devices anda

![2.0 - Edit Devices](../../.gitbook/assets/191127_0002.png)

### Penjelasan Setting Device Delay :

* Jika anda mengirim 100 pesan, dan anda set 1 pesan setiap 10 menit, sistem akan mengirim pesan sesuai settingan anda
* 100 pesan / 1 pesan untuk 10 menit \(100x jalan\)
* Perhitungannya Seperti ini :

  * 60 menit / 10 menit = 6
  * Maka sistem kami akan mengirimkan pesan hanya 6x dalam 1 jam ke 6 nomor berbeda.

## Mengatur Notifikasi Pesan Masuk/Quota Hampir Habis

* Klike pada menu `Subscription`, kemudian klik tombol `Edit`

![2.1 - Edit Button Subscription](../../.gitbook/assets/191127_0003.png)

* Maka akan muncul pop up edit

![2.12 - Edit Subscription](../../.gitbook/assets/191127_0004.png)

### Penjelasan Edit Subscription

* **Whatsapp Number Noify**: Nomor Tujuan Whatsapp Notifikasi Pesan Masuk/Low Quota \(Mengaktifkan fitur ini, akan memotong quota pesan anda\). _**Kosongkan untuk menonaktifkan fitur ini.**_
* **Email Notify**: Email Tujuan Notifikasi Pesan Masuk/Low Quota. _**Kosongkan untuk menonaktifkan fitur ini.**_
* **Low Quota Notify**: Batas Notifikasi Low Quota akan dikirim
* **Low Quota Notify Send Via**: anda bisa mengatur apakah notifikasi dikirim via email saja atau email dan whatsapp.



