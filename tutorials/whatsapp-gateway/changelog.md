# Changelog

## 0.8 - 2019-11-01

### Changed

* Fixed error Sync to Google Contact when added New Contact or Import Contact.
* Fixed error when running delete all contact, if contact lists is empty.
* Increase the number of contact synchronizations from 30 to 45 per minute.
* improve contact synchronization time from the contacts import feature.

## 0.7 - 2019-10-27

### Added

* When user send a message, and if number not found in contact list, system will generate random name and save to contact and sync to google contact
* If user import contact list, system will running in background to sync from contact to google contact
* If user import contact list with same username or name, system will generate random name too.
* Now user can set limit bulk message in "device" menu and set privacy
  * Limit bulk will set send message each group, example: 
    * if you send 100 message, and you set limit 2 message for 10 minutes, system will send with your settings
    * 100 message / 2 message for 10 minutes \(50x times running\)
    * So system will run each 10 minutes send 2 message for you directly in background
  * If privacy set to "Active", all message out, will not save on our database

### Changed

* Another bug fixed

## 0.6 - 2019-09-25

### Added

* Bulk Message Delay For Protected From Blocked
* Bulk Message History, With this feature, you can check when you're message will be send by our system
* Each Bulk Message Will Grouped With xx Message and send with xx Every Minutes, _**at this time, we set 15 message/15minutes.**_
* Now User Can Delete Message Inbox and Outbox and Contact List Directly

### Changed

* if Group Contact Has Been Delete, Contact Not Will Deleted **\[This has been fixed\]**
* When import contact, there no loading action showing **\[This has been fixed\]**
* Another Bug Fixed

## 0.5 - 2019-09-21

### Added

* Device Status and Restart

### Changed

* Another Bug Fixed

## 0.4 - 2019-09-13

### Changed

* Fixed Bugs When Send Message
* Another Bug Fixed

## 0.3 - 2019-09-09

### Changed

* Fixed Admin Template
* Another Bug Fixed

## 0.2 - 2019-09-05

### Added

* Blocked Feature
* Notification

### Changed

* User Template
* Another Bug Fixed

## 0.1 - 2019-09-02

### Added

* First Release



