# Integrasi WSGateway ke WHMCS

## Fitur Module WHMCS WSGateway

1. Auto Import Phone Number to Custom Fields
2. Using Dynamic Fields Function
3. Send Bulk Message Based by Status Client
4. Send Bulk Message Based Product Client With Domains and Status Product
5. Auto Send Welcome Message \(Register/Login/Edit\)
6. Auto Send Invoice Message \(Created/Reminder/Overdue 1-2-3/Confirmation\)
7. Auto Send Module \(Created/Suspend/Unsuspend/Terminated\)
8. Auto Send Support Ticket \(Open by User or Admin/Reply by User or Admin\)
9. Auto Send to Admin Notif \(Login/New Order/Invoice Confirmation\)
10. Log System
11. Support PHP 5.6/7.2 \(Tested WHMCS 7.7.1 + PHP 5.6 and WHMCS 7.7.1 + PHP 7.2\)

## Screenshoot

{% tabs %}
{% tab title="Default Pages" %}
![](../../.gitbook/assets/01.png)
{% endtab %}

{% tab title="Welcome Notif" %}
![](../../.gitbook/assets/02.png)
{% endtab %}

{% tab title="Invoice Notif" %}
![](../../.gitbook/assets/03.png)
{% endtab %}

{% tab title="Product Notif" %}
![](../../.gitbook/assets/04.png)
{% endtab %}

{% tab title="Support Ticket Notif" %}
![](../../.gitbook/assets/05.png)
{% endtab %}

{% tab title="Admin Notif" %}
![](../../.gitbook/assets/06.png)
{% endtab %}
{% endtabs %}

## Install Module Addons WSGateway di WHMCS

* Download files dari Website Kami
* Ekstrack, dan upload sesuai path foldernya
  * File pada Folder /modules/addons maka di upload ke folder website anda sesuai urutan folder tersebut yaitu /modules/addons
  * File pada Folder /includes/hooks maka di upload ke folder website anda sesuai urutan folder tersebut yaitu /includes/hooks
* Pada WHMCS Admin anda, ke menu "Setup" =&gt; "Addons Module" =&gt; "PFN WSGateway"

![](../../.gitbook/assets/selection_000042.png)

* Klik "Active" untuk mengaktifkan module
* Beri hak akses module sesuai kebutuhan anda dan klik "save change"

![](../../.gitbook/assets/selection_000043.png)

* Untuk mengakses module, dari menu "Addons" =&gt; "PFN WSGateway"

### Setting Custom Field Nomor Whatsapp

Custom Fields ini digunakan untuk menyimpan nomor whatsapp, jika form kosong, maka pesan akan diabaikan \(tidak dikirim\)

* Masuk ke menu "Setup" =&gt; "Custom Client Fields"
* Isi form seperti ini :
  * Field Name: Nomor Hp
  * Field Type: Text Box
  * Centang pada "Show On Order Form"
* Save Change untuk menyimpan data tersebut

![](../../.gitbook/assets/selection_000044.png)

### Setup Dasar Addons Module

* Untuk Form "Api URL" / "Device ID" / "Api Token" akan didapatkan ketika anda melakukan order, anda hanya perlu menyalin dan menyimpan detail tersebut ke form pada addons wsgateway
* Nomor Hp Admin = Berfungsi untuk menerima notifikasi seperti login/order baru dan invoice confirmation
* Username Admin = Berfungsi untuk memfilter admin login notif, sehingga bisa memilih admin mana yang akan di capture dan dikirim notif login.
* Custom Field ID = Pilih sesuai yang dibuat sebelumnya
* Klik "Simpan Perubahan Setup Dasar" untuk menyimpan data

![](../../.gitbook/assets/integrasi-wsgateway-ke-whmcs-v1-1.png)

### Test Kirim Pesan

Pada halaman paling bawah addons, ada form test kirim pesan, masukkan nomor whatsapp anda, jika settingan anda benar, maka anda akan segera menerima pesan via whatsapp melalui whmcs.

### Import Kontak Pada Phone Number ke Custom Fields

Pada halaman paling bawah addons, ada tombol "Import Contact", anda hanya perlu mengklik 1x, maka semua data pelanggan anda yang telah mengisi form "Phone Number" akan langsung di import ke Custom Fields yang kita buat sebelumnya.

![](../../.gitbook/assets/selection_000046.png)

#### Log System

Kami menambahkan fitur Log System untuk mempermudah proses debug dan tracking module, anda bisa mengecek setiap action yang dilakukan module melalui _**"Utilities" =&gt; "Logs" =&gt; "Activity Log"**_

## Changelog Addons WHMCS

### v1.1 - 2019-10-1

* Installasi Lebih Mudah Karena Hanya Upload 1 Folder ke Addons Saja
* Tampilan kini basis per-menu, sehingga ketika save data, tidak pindah ke menu lain.
* Bisa pilih notifikasi admin mana yang akan di capture jika login, misalnya jika punya banyak admin, dan hanya ingin notif jika adminsatu yg login saja \(fields: Username Admin\)

### v1.0 - First Release

