# Tips Menghindari Nomor Terblokir

## Whatsapp FAQ

### Melihat pesan "Nomor telepon Anda diblokir dari menggunakan WhatsApp. Hubungi dukungan untuk bantuan."

{% hint style="info" %}
Jika akun Anda diblokir, Anda akan menerima pesan berikut di dalam WhatsApp:

“Nomor telepon Anda diblokir dari menggunakan WhatsApp. Hubungi dukungan untuk bantuan.”

Harap diperhatikan bahwa kami memblokir akun yang kami yakini aktivitas akunnya telah melanggar [Ketentuan Layanan](https://www.whatsapp.com/legal/#TOS) kami.

Harap tinjau bagian "Penggunaan Layanan Kami yang Diperbolehkan" dari Ketentuan Layanan dengan hati-hati untuk mempelajari selengkapnya mengenai penggunaan WhatsApp yang patut dan aktivitas-aktivitas yang melanggar Ketentuan Layanan kami.

Ada kemungkinan kami tidak memberi peringatan sebelum memblokir akun Anda. Jika menurut Anda akun Anda tidak seharusnya diblokir, silakan kirim email kepada kami dan kami akan meninjau kasus Anda.
{% endhint %}

Ref: [https://faq.whatsapp.com/en/android/23154266/?category=5245246&lang=id](https://faq.whatsapp.com/en/android/23154266/?category=5245246&lang=id)

### Whatsapp Terms of Services \(TOS\)

{% hint style="info" %}
**Legal and Acceptable Use**. You must access and use our Services only for legal, authorized, and acceptable purposes. You will not use \(or assist others in using\) our Services in ways that: \(a\) violate, misappropriate, or infringe the rights of WhatsApp, our users, or others, including privacy, publicity, intellectual property, or other proprietary rights; \(b\) are illegal, obscene, defamatory, threatening, intimidating, harassing, hateful, racially, or ethnically offensive, or instigate or encourage conduct that would be illegal, or otherwise inappropriate, including promoting violent crimes; \(c\) involve publishing falsehoods, misrepresentations, or misleading statements; \(d\) impersonate someone; \(e\) involve sending illegal or impermissible communications such as bulk messaging, auto-messaging, auto-dialing, and the like; or \(f\) involve any non-personal use of our Services unless otherwise authorized by us.
{% endhint %}

Ref: [https://www.whatsapp.com/legal/\#terms-of-service](https://www.whatsapp.com/legal/#terms-of-service)

## Menghindari Pemblokiran Nomor Whatsapp?

Seperti yang kita lihat pada 2 FAQ yang kami salin dari situs whatsapp tersebut, yang intinya kita tidak bisa melakukan atau mengirim pesan massal menggunakan whatsapp, maka akan menyebabkan nomor terblokir.

**Jika nomor terblokir, maka tidak ada yang bisa kami lakukan selain mengganti nomor whatsapp anda pada sehingga dapat digunakan kembali. Seberapa lama nomor dapat bertahan dari pemblokiran semua tergantung bagaimana cara anda menggunakan whatsapp gateway tersebut.**

Tetapi untuk beberapa alasan yang tidak diketahui, ini masih bisa kita hindari bukan berarti anti-banned ya.

### Tips Agar Tidak Terbaca Sebagai BOT/Robot

* Pada sistem kami WSGateway v0.7 telah ditambahkan fitur sinkron antara sistem kami dan kontak pada HP yang anda gunakan. Hal ini sedikit banyak, menurunkan angka pemblokiran karena kita mengirim pesan dengan kontak yang telah tersimpan pada HP yang digunakan.
* **Gunakan Delay Pesan, Ikuti Tutorial Berikut** [**\[DI SINI\]**](https://docs.premiumfast.net/tutorials/whatsapp-gateway/cara-penggunaan-wsgateway#membuat-delay-pesan-ketika-mengirim-pesan-massal)\*\*\*\*
  * Mengirim pesan secara terus menerus dengan kata yang sama dalam waktu berdekatan, ini terindakasi untuk terblokir, maka hindari pengiriman pesan terlalu cepat jika anda ingin nomor anda bertahan lebih lama.
  * Dengan delay pesan, anda bisa membatasi pengiriman pesan dalam waktu berdekatan, sehingga tampak lebih natural, apalagi jika anda mengkobinasikan kata-kata pada pesan anda, sehingga nomor anda akan bertahan lebih lama dari pemblokiran.
* Hindari mengirim pesan sama secara terus menerus ke nomor baru/belum pernah berinteraksi.
  1. Jika anda ingin mengirim pesan masal kami sarankan untuk menggunakan kata-kata yang berbeda di setiap pengiriman pesan massal tersebut.
  2. Anda bisa membuat group per-nomor \(Contact Group\) pada Panel WSGateway, misalnya isi 1 Contact Group dengan 25 Nomor, Jika anda mempunyai 100 Nomor maka akan ada 4 Group berbeda. dan jika pesan dikirim, usahakan ada perbedaan beberapa kata di tiap group nya.

{% hint style="info" %}
### Contoh Group 1 :

Hai,  
Kami informasikan bahwa saat ini kami sedang melakukan perbaikan. Mohon kembali lagi _**10 menit kedepan**_ atau bisa menghubungi kami di email john\_doe@test.com.  
  
Hormat Kami,  
John Doe

### Contoh Group 2 :

Hai,  
Kami informasikan bahwa saat ini kami sedang melakukan perbaikan. Mohon kembali lagi _**15 menit lagi**_ atau bisa menghubungi kami di email john\_doe@test.com.  
  
Hormat Kami,  
John Doe
{% endhint %}

## Hasil dari Tips diatas

* Beberapa tips tersebut yang kami gunakan selama ini hampir 1 tahun menggunakan whatsapp gateway, nomor kami tidak pernah terblokir \(pernah terjadi pergantian nomor sebelumnya, dikarenakan perpindahan whatsapp pada HP lama ke HP baru tetapi pesan kode login whatsapp tidak kunjung masuk, sehingga nomor kami ganti\).
* Menggunakan whatsapp gateway seperti whatsapp biasa dengan tidak mengirim pesan yang seperti SPAM dapat memperpanjang umur whatsapp atau menghindari nomor whatsapp anda dari pemblokiran.
* Sama seperti email. informasikan bagaimana cara user untuk membatalkan atau unsubscribe pengiriman whatsapp agar user tidak merasa SPAM dan melakukan Report Nomor Whatsapp Anda.
* Terkadang nomor anda terblokir bukan karena pengiriman masal atau mengirim pesan whatsapp, tetapi ada beberapa orang yang melaporkan nomor whatsapp anda, sehingga nomor anda menjadi terblokir.

