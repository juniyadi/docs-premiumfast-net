# REST API

## SDK Library

Jika anda kesulitan menggunakan RAW REST API, anda bisa menggunakan Library yang kami telah sediakan pada di link dibawah ini:

* [PHP](https://github.com/Premium-Fast-Network/wsgateway-php)
* [NodeJS](https://github.com/Premium-Fast-Network/wsgateway-node)

## Catatan Penting Sebelum Memulai

* Di sini kami tidak memberikan full nama url API Endpoint / API URL, sebagai gantinya kami mengganti menjadi `example.com`
* Ketika anda berlangganan produk Whatsapp Gateway kami, kami akan memberikan FULL URL Login Panel beserta URL API Endpointnya, ganti `example.com` sesuai alamat API Endpoint yang kami berikan
* Pastikan untuk mengganti data sesuai data yang benar, seperti :
  * YOUR-TOKEN-HERE
  * YOUR-DEVICE-ID
  * YOUR-WHATSAPP-NUMBER
* Jika anda tidak mengetahui TOKEN dan DEVICE-ID, silahkan cek email detail whatsapp kami pada bagian **"\[PENTING\] Penggunaan Whatsapp Gateway dan REST API"** atau **jika anda sedang melakukan trial, TOKEN dan DEVICE-ID  kami lampirkan langsung pada detail trial beserta URL API Endpoint.**

## Api End Point

* `/api/v1`

{% api-method method="post" host="" path="/message/send" %}
{% api-method-summary %}
Send Message
{% endapi-method-summary %}

{% api-method-description %}
Kirim pesan ke nomor whatsapp.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-headers %}
{% api-method-parameter name="Accept" type="string" required=true %}
application/json
{% endapi-method-parameter %}

{% api-method-parameter name="Authentication" type="string" required=true %}
`Bearer YOUR-TOKEN-HERE`
{% endapi-method-parameter %}
{% endapi-method-headers %}

{% api-method-form-data-parameters %}
{% api-method-parameter name="deviceid" type="string" required=true %}
Your Device ID.
{% endapi-method-parameter %}

{% api-method-parameter name="number" type="number" required=true %}
Whatsapp Number Destionation.
{% endapi-method-parameter %}

{% api-method-parameter name="message" type="string" required=true %}
Your Message.
{% endapi-method-parameter %}
{% endapi-method-form-data-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Cake successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
{
    "code": 200,
    "message": "success, message will be send in background."
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

### \# Example Send Message

{% tabs %}
{% tab title="Curl" %}
```bash
curl -s \
	-X POST \
	-H "Accept: application/json" \
	-H "Authorization: Bearer YOUR-TOKEN-HERE" \
	-d "deviceid=YOUR-DEVICE-ID" \
	-d "number=YOUR-WHATSAPP-NUMBER" \
	-d "message=hai ini hanya test" \
	https://example.com/api/v1/message/send
```
{% endtab %}

{% tab title="PHP" %}
```php
<?php

$postData = array(
	'deviceid' => 'YOUR-DEVICE-ID',
	'number' => '08xxxxxxxxx',
	'message' => 'hai ini hanya test',
);


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://example.com/api/v1/message/send');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Authorization: Bearer YOUR-TOKEN-HERE';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);

print_r($result);
```
{% endtab %}
{% endtabs %}

{% api-method method="post" host="" path="/message/sendbulk" %}
{% api-method-summary %}
Send Bulk Message
{% endapi-method-summary %}

{% api-method-description %}
Kirim pesan whatsapp ke banyak nomor.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-headers %}
{% api-method-parameter name="Accept" type="string" required=true %}
application/json
{% endapi-method-parameter %}

{% api-method-parameter name="Authentication" type="string" required=true %}
`Bearer YOUR-TOKEN-HERE`
{% endapi-method-parameter %}
{% endapi-method-headers %}

{% api-method-form-data-parameters %}
{% api-method-parameter name="deviceid" type="string" required=true %}
Your Device ID.
{% endapi-method-parameter %}

{% api-method-parameter name="number" type="array" required=true %}
Multiple Whatsapp Number Destionation.
{% endapi-method-parameter %}

{% api-method-parameter name="message" type="string" required=true %}
Your Message.
{% endapi-method-parameter %}
{% endapi-method-form-data-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Cake successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
{
    "code": 200,
    "message": "success, all message in waiting list."
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

### \# Example Send Bulk Message

{% tabs %}
{% tab title="Curl" %}
```bash
curl -s \
	-X POST \
	-H "Accept: application/json" \
	-H "Authorization: Bearer YOUR-TOKEN-HERE" \
	-d '{"deviceid": "YOUR-DEVICE-ID", "number": [ 08xxxxxxxxx1, 08xxxxxxxxx2 ], "message": "hai ini hanya test"}' \
	https://example.com/api/v1/message/sendbulk
```
{% endtab %}

{% tab title="PHP" %}
```php
<?php

$postData = array(
	'deviceid' => 'YOUR-DEVICE-ID',
	'number' => array(
		'08xxxxxxxxx1', '08xxxxxxxxx2',
	),
	'message' => 'hai ini hanya test',
);


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://example.com/api/v1/message/sendbulk');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Authorization: Bearer YOUR-TOKEN-HERE';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);

print_r($result);
```
{% endtab %}
{% endtabs %}

{% api-method method="post" host="" path="/message/sendgroup/{groupID}" %}
{% api-method-summary %}
Send Message to Group
{% endapi-method-summary %}

{% api-method-description %}
Kirim pesan whatsapp ke group.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-headers %}
{% api-method-parameter name="Accept" type="string" required=true %}
application/json
{% endapi-method-parameter %}

{% api-method-parameter name="Authentication" type="string" required=true %}
`Bearer YOUR-TOKEN-HERE`
{% endapi-method-parameter %}
{% endapi-method-headers %}

{% api-method-query-parameters %}
{% api-method-parameter name="groupID" type="string" required=false %}
Contact Group ID.
{% endapi-method-parameter %}
{% endapi-method-query-parameters %}

{% api-method-form-data-parameters %}
{% api-method-parameter name="deviceid" type="string" required=true %}
Your Device ID.
{% endapi-method-parameter %}

{% api-method-parameter name="message" type="string" required=true %}
Your Message.
{% endapi-method-parameter %}
{% endapi-method-form-data-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Cake successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
{
    "code": 200,
    "message": "success, all message in waiting list."
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

### \# Example Send Message to Group

{% tabs %}
{% tab title="Curl" %}
```bash
curl -s \
	-X POST \
	-H "Accept: application/json" \
	-H "Authorization: Bearer YOUR-TOKEN-HERE" \
	-d '{"deviceid": "YOUR-DEVICE-ID", "message": "hai ini hanya test"}' \
	https://example.com/api/v1/sendgroup/{groupID}
```
{% endtab %}

{% tab title="PHP" %}
```php
<?php

$postData = array(
	'deviceid' => 'YOUR-DEVICE-ID',
	'message' => 'hai ini hanya test',
);


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://example.com/api/v1/message/sendgroup/{groupID}');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Authorization: Bearer YOUR-TOKEN-HERE';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);

print_r($result);
```
{% endtab %}
{% endtabs %}

