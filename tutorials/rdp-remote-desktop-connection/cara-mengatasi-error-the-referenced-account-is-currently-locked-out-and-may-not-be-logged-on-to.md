# 1.4 - Cara Mengatasi Error "The Referenced Account is Currently locked out"

Berikut kami ingin menginformasikan bahwa error "The Referenced Account is Currently locked out and may not be logged on to" yang sering terjadi pada RDP ini adalah kesalahan saat logout akun, atau multi login.

Karena pada dasarnya untuk RDP hanya dibolehkan menggunakan oleh 1 orang dalam 1 waktu, ketika 1 user di gunakan secara bersamaan. Maka user tersebut yang login pertama akan LOGOUT, dan user aktif berpindah ke user yang logn kedua.

Jika anda tidak melakukan MULTI LOGIN, berarti ada yang salah dari penggunaan anda.

Baik disini saya akan informasikan cara memperbaikinya :

* Setelah anda mendapatkan Error Login seperti ini : The Referenced Account is Currently locked out and may not be logged on to, tunggu 5 hingga 15 menit \(user login akan ter-reset secara automatis\)
* Kemudian login ke RDP, maka semua berjalan dengan lancar.
* Saat anda ingin logout RDP, jangan menggunakan fitur LOGOUT dari akun,tetapi langsung saja meng-click tombol X \(Close\) pada pojok kanan atas RDP

![](../../.gitbook/assets/screenshot_754.jpg)

* Jika anda menggunakan fitur LOGOUT akun pada RDP, artinya user yang anda gunakan sedang logout, makanya saat anda login kembali akan muncul error "The Referenced Account is Currently locked out and may not be logged on to"
* Permasalahan FIX

### Catatan tambahan

* Ketika anda login ke RDP dan salah memasukan password selama 3 kali berturut-turut, hal tersebut dapat menyebabkan akun tidak bisa login selama -+ 30 menit, maka setelah 30 menit, silahkan login ke rdp kembali dengan password yang benar \(hal ini dilakukan demi melindungi server dari serangan brute force\)

