# 1.0 - Cara Login Ke Semua RDP

Berikut cara login ke RDP \(Remote Desktop Protocol\) dari beberapa OS \(Operasi Sistem\)

### Untuk Windows 7

1. Klik Start Menu
2. Ketik pada kotak pencarian: `Remote Desktop Connection`
3. Kemudian, klik aplikasi `Remote Desktop Connecion`
4. Masukan `IP SERVER` 
5. Klik `Connect`
6. Kemudian klik pada pilihan `Use Another Account`
7. Masukan `Username` dan `Password`
8. Klik `Connect`
9. Selesai

### Untuk Windows 8 & Windows 10 

1. Cari Aplikasi `Remote Desktop Connection` dan buka aplikasi tersebut
2. Masukan `IP SERVER` 
3. Klik `Connect`
4. Masukan `Username` dan `Password`
5. Klik `Connect`
6. Selesai

### Untuk Macbook

1. Download dan Install `Microsoft Remote Desktop` Dari App Store.
2. Klik New dan Masukkan Nama RDP: Terserah apa yang anda inginkan \(RDP1 misalnya\) PC Name: `IP Server` ****Username: `UsernameRDP` ****Password: `PasswordRDP` 
3. Kemudian Klik Close \( akan muncul popup simpan data, dan klik save \)
4. Klik kanan pada data yang tersimpan tadi atau langsung klik 2x pada data tersebut.
5. Selesai



