# 1.5 - Cara Menggunakan Proxy di Browser

Jika anda tidak bisa akses website karena terblokir, anda bisa menggunakan salah satu proxy yang kami sediakan. proxy ini bisa anda gunakan untuk bypass beberapa website tersebut \(contoh: zippyshare\).

## List Proxy :

* Login Detail :
  * Port : **1080**
  * Type: **SOCKS v5**
  * _**Tidak Menggunakan Username dan Password**_
* URL/Host Proxy :
  * proxy-satu.premiumfast.net
  * proxy-dua.premiumfast.net
  * proxy-tiga.premiumfast.net
* _**Proxy hanya bisa digunakan pada produk**_ [_**RDP**_](https://premiumfast.net/product/rdp-user)_\*\*\*\*_

## Menggunakan Proxy di Firefox

![](../../.gitbook/assets/191020_0004.png)

