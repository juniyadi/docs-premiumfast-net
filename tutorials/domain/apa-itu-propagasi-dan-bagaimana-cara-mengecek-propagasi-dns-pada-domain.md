# 1.1 - Apa itu Propagasi dan Bagaimana Cara Mengecek Propagasi DNS Pada Domain

Propagasi Domain/ Masa Propagasi Domain adalah proses dimana domain membutuhkan waktu untuk domain tersambung ke server hosting.

Masa Propagasi ini bervariasi mulai dari hitungan menit, jam, atau bahkan berhari-hari.

Pada Domain Propagation Period \(Masa propagasi domain\) ini tergantung ISP \(Internet Service Provider\) dari masing-masing internet yang anda gunakan. Biasanya para ISP mengupdate DNS mereka 1×24 jam, 2×24 jam, bahkan 3×24 jam. Tetapi ada juga ISP yang mengupdate DNS secara cepat \(Tentu ini ketika anda menggunakan default DNS bawaan provider ISP Internet yang anda gunakan\).

## Bagaimana Cara Mengecek Propagasi Domain Saya?

* Kunjungi website berikut : [https://intodns.com/](https://intodns.com/)
* Masukkan alamat website anda pada kolom _**Domain Name kemudian**_ klik _**Report**_

![1.0 &#x2013; Halaman Awal IntoDNS](../../.gitbook/assets/selection_00824.png)

* IntoDNS akan mengecek domain anda, apakah DNS bisa diakses atau tidak, jika berhasil, maka akan muncul seperti ini

![1.1 &#x2013; Report dari IntoDNS](../../.gitbook/assets/selection_00823.png)

* Yang paling penting adalah pada _**Domain NS records**_, jika anda melakukan perubahan Nameserver, pastikan untuk mengecek bagian ini, jika sudah sesuai berarti Progpagasi sudah selesai.
* Terakhir, test ping melalui CMD atau Terminal Linux anda \(disini saya menggunakan terminal linux\), command ; _**ping domainanda.com**_

![1.2 &#x2013; Ping Domain pfndomain.xyz](../../.gitbook/assets/selection_00825.png)

* Jika seperti gambar 1.2 berarti website anda sudah bisa diakses, silahkan coba buka melalui browser.

![1.3 &#x2013; Domain sudah bisa diakses](../../.gitbook/assets/selection_00826.png)

* Selesai

