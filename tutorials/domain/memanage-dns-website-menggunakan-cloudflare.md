# 1.0 - Memanage DNS Website Menggunakan Cloudflare

Cloudflare merupakan system managemen DNS yang menyediakan berbagai macam fitur walaupun dalam mode gratis. Hal ini sangatlah bermanfaat bagi website anda baik dalam skala kecil atau besar. Kelebihan cloudflare seperti :

1. Manage DNS
2. Layanan CDN \(Content Delivery Network\)
3. Gratis DDOS Protection
4. Caching Static File \(Images/css/js/other\)
5. Free SSL Selamanya
6. dll

Pertama, jika anda belum mempunyai akun, daftar terlebih dahulu kemudian setelah daftar baru anda dapat menggunakan layanan tersebut.

## Mendaftar di Cloudflare

* Isi form Email dan Password pada link pendaftaran =&gt; [https://dash.cloudflare.com/sign-up](https://dash.cloudflare.com/sign-up)

![1.0 &#x2013; Mendaftar Cloudflare](../../.gitbook/assets/selection_00801.png)

* Cek email anda, kemudian klik link konfirmasi untuk mengaktifkan akun cloudflare

![1.1 &#x2013; Email Konfirmasi](../../.gitbook/assets/selection_00802.png)

Setelah mengklik link pada email, akun anda telah dikonfirmasi. Kemudian kembali ke cloudflare, login dengan detail email dan password yang anda daftar tadi. link : [https://dash.cloudflare.com/login](https://dash.cloudflare.com/login)

* Untuk menambahkan domain anda ke cloudflare, klik tombol _**Add Site**_

![1.2 &#x2013; Halaman awal website cloudflare](../../.gitbook/assets/selection_00804.png)

* Masukkan alamat website anda dan klik _**Add Site**_ dan tunggu hingga proses check domain selesai

![1.3 &#x2013; Menambahkan website](../../.gitbook/assets/selection_00805.png)

* Klik _**Next**_ untuk melanjutkan

![1.4 &#x2013; Melanjutkan Proses Setup](../../.gitbook/assets/selection_00806.png)

* Kemudian akan muncul halaman paket seperti ini

![1.5 &#x2013; Paket Layanan Cloudflare](../../.gitbook/assets/selection_00807.png)

* Klik pada table _**FREE**_ untuk menggunakan cloudflare secara gratis dan pastikan centang sudah berada pada kolom table tersebut, kemudian klik _**Confirm Plan**_

![1.6 &#x2013; Pilih Paket Layanan Free Cloudflare](../../.gitbook/assets/selection_00808.png)

* Akan muncul pop-up, kemudian klik _**Confirm**_

![1.7 &#x2013; Konfirmasi Paket Layanan](../../.gitbook/assets/selection_00809.png)

* Kemudian anda akan diarahkan ke halaman pengaturan DNS untuk domain anda, scrool kebawah untuk melihat lengkapnya.

![1.8 &#x2013; Pengaturan DNS Cloudflare](../../.gitbook/assets/selection_00810.png)

* Standarnya settingan DNS anda harus seperti dibawah ini.

![1.9 &#x2013; Pengaturan DNS Domain](../../.gitbook/assets/selection_00811.png)

Keterangan Gambar diatas, gambar diatas hanya contoh, rubah sesuai data website dan ip anda..!!

* _**pfndomain.xyz**_ =&gt; ini adalah alamat website anda.
* _**123.123.123.123**_ =&gt; ini adalah ip server website anda atau ip vps server anda.
* _**Tombol Cloud \(Abu-Abu / Kuning\)**_ =&gt; ini berfungsi untuk mengaktifkan CDN \(Content Delivery Network\) pada domain anda, untuk mengaktifkan tinggal klik saja dan warna berubah jadi kuning, jika abu-abu, berati anda tidak menggunakan fitur CDN

Jika sudah selesai, langsung klik _**Continue**_.

* Selanjutnya, akan muncul halaman untuk perubahan Nameserver pada domain anda.
* _**Salin**_ kedua _**nameserver**_ pada table sebelah kanan

![2.0 &#x2013; Salin Nameserver](../../.gitbook/assets/selection_00812.png)

* Jika anda menyalin nameserver, scroll kebawah dan klik _**Continue**_

![2.1 &#x2013; Melanjutkan tahap Selanjutnya](../../.gitbook/assets/selection_00813.png)

* Kemudian anda akan diarahkan kehalaman depan.. tapi .. tapi .. tunggu dahulu, proses belum sepenuhnya selesai.

![2.2 &#x2013; Halaman awal domain](../../.gitbook/assets/selection_00814.png)

* Kita baru saja menyelesaikan setup domain pada cloudflare, selanjutnya kita harus _**merubah nameserver domain anda**_ sesuai dengan yang kita salin tadi pada _**gambar 2.0**_

## **Merubah Nameserver Domain Pada Premium Fast Network**

_Catatan sebelum memulai :   
Jika anda menggunakan domain dari website lain, kami sarankan untuk menanyakan provider ditempat anda membeli untuk tutorial **cara merubah nameserver domain** yang anda gunakan…!!!_

* Login ke website kami : [https://manage.premiumfast.net/login.php](https://manage.premiumfast.net/login.php)
* Klik pada Menu : _**Domains =&gt; List Domain Saya**_

![2.3 &#x2013; Halaman Utama Manage PFN](../../.gitbook/assets/selection_00815.png)

* Pilih _**Domain**_ sesuai yang anda daftar pada cloudflare tadi \(Sesuai contoh pada _**gambar 1.9**_ kami mendaftarkan domain _**pfndomain.xyz**_ pada cloudflare\)

![2.4 &#x2013; Halaman List Domain Saya](../../.gitbook/assets/selection_00816.png)

* Klik pada Panah Kebawah pada baris domain yang ingin anda rubah dan klik Manage Nameservers

![2.5 &#x2013; Pilih Opsi Manage Domain](../../.gitbook/assets/selection_00817.png)

* Akan muncul halaman nameserver seperti ini

![2.6 &#x2013; Halaman Manage Nameservers](../../.gitbook/assets/selection_00818.png)

* Pilih pada : _**Use custom nameservers \(enter below\)**_Kemudian masukkan _**Nameserver 1**_ dan _**Nameserver 2**_ sesuai dengan gambar 2.0 sebelumnya. _**\(PENTING..!! Jangan rubah sesuai gambar, karena nameserver ini digenerate oleh cloudflare, sehingga beda domain, bisa beda juga nameservernya\)**_
* Jika anda lupa nameserver cloudflarenya, anda bisa cek kembali di website cloudflare

![2.7 &#x2013; Merubah Nameserver Domain](../../.gitbook/assets/selection_00819.png)

* Jika sudah sesuai datanya, klik tombol _**Change Nameservers,**_ Jika berhasil maka data akan berubah

![2.8 &#x2013; Sukses Mengganti Nameserver](../../.gitbook/assets/selection_00820.png)

* Selanjutnya, anda harus menunggu propagansi dns domain anda agar sepenuhnya berubah ke server cloudflare
* Proses ini biasanya mamakan waktu 24-72 jam paling lama, tetapi biasanya kurang dari 30 menit, domain anda sudah aktif dan bisa diakses.
* Jika domain anda sudah aktif, anda akan mendapatkan notifikasi via email dari cloudflare.

![2.9 &#x2013; Email Notifikasi Domain Aktif Cloudflare](../../.gitbook/assets/selection_00822.png)

