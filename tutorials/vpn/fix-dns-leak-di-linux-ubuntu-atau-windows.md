# Fix DNS Leak Di Linux Ubuntu atau Windows

## Apa itu DNS Leak?

Secara garis besar, DNS Leak merupakan sebuah proses dimana setiap request DNS Query di inject oleh DNS ISP. Sehingga ketika anda mengakses situs yang terblokir oleh DNS, tetap tidak terbuka meskipun anda sudah menggunakan VPN pada Komputer Anda.

Sehingga ini menyebabkan VPN seperti tidak bisa digunakan sama sekali atau efektifitasnya tidak ada, karena tentu saja salah satu fitur utama yang paling di cari selain keamanan menggunakan VPN yaitu bisa membuka situs terblokir.

## Bagaimana cara mengecek DNS Leak?

Silahkan mengunjungi web berikut dan klik `Standard Test`

* [https://www.dnsleaktest.com/](https://www.dnsleaktest.com/)

![1.0 - Hasil Standard Test DNS](../../.gitbook/assets/fix-dns-leak-di-linux-ubuntu-atau-windows-1-0.png)

## Test dengan VPN Default

* Login ke VPN seperti biasa, kemudian ke web dnsleaktest kembali

![1.1 - Test DNSLeak dengan VPN](../../.gitbook/assets/fix-dns-leak-di-linux-ubuntu-atau-windows-1-1.png)

* Pada gambar 1.1 terlihat bahwa kita sudah menggunakan VPN tetapi DNS tetap tidak berubah, padahal pada VPN tersebut sudah di inject DNS dari Google tetapi tidak mempan. Ini adalah result sebelum saya menggunakan perubahan configurasi.
* Kemudian kita akan test membuka website `vimeo.com` 

![1.2 - Test Buka Web Vimeo](../../.gitbook/assets/fix-dns-leak-di-linux-ubuntu-atau-windows-1-2.png)

* Dari hasil gambar 1.2 .. ternyata hanya loading saja, web tidak terbuka.

## Update Konfigurasi

### Linux Ubuntu

* Ubah Konfigurasi Network Manager, Jalankan Command `/etc/NetworkManager/NetworkManager.conf`, kemudian hapus baris `dns=default` , sehingga menjadi seperti ini :  

```bash
$ cat /etc/NetworkManager/NetworkManager.conf
[main]
plugins=ifupdown,keyfile

[ifupdown]
managed=false

[device]
wifi.scan-rand-mac-address=no
```

* Cek Interfaces Internet anda dengan command `ip a`

```bash
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether xx:xx:xx:xx:xx:xx brd ff:ff:ff:ff:ff:ff
    inet 192.168.8.100/24 brd 192.168.8.255 scope global dynamic noprefixroute enp1s0
       valid_lft 80094sec preferred_lft 80094sec
    inet6 fe80::a10f:8d3b:d471:59ab/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: wlp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether xx:xx:xx:xx:xx:xx brd ff:ff:ff:ff:ff:ff
```

Pada hasil command diatas, komputer saya mempunyai 2 interfaces network yaitu `enp1s0` sebagai LAN dan `wlp2s0` sebagai WiFi. Dan saat ini yang terhubung ke internet adalah `enp1s0`.

* Ubah settingan DHCP di `/etc/dhcp/dhclient.conf` kemudian masukkan kode berikut diakhir file :

{% code title="/etc/dhcp/dhclient.conf" %}
```text
# ganti enp1s0 sesuai dengan network interfaces anda
# 1.1.1.1 1.0.0.1 merupakan DNS dari Cloudflare
interface "enp1s0" {
        prepend domain-name-servers 1.1.1.1, 1.0.0.1;
}
```
{% endcode %}

* Restart Network Manager dengan `sudo systemctl restart network-manager`
* Tambahkan file berikut ke `/etc/openvpn/update-resolv-conf`

{% code title="/etc/openvpn/update-resolv-conf" %}
```bash
#!/bin/bash
# 
# Parses DHCP options from openvpn to update resolv.conf
# To use set as 'up' and 'down' script in your openvpn *.conf:
# up /etc/openvpn/update-resolv-conf
# down /etc/openvpn/update-resolv-conf
#
# Used snippets of resolvconf script by Thomas Hood and Chris Hanson.
# Licensed under the GNU GPL.  See /usr/share/common-licenses/GPL. 
# 
# Example envs set from openvpn:
#
#     foreign_option_1='dhcp-option DNS 193.43.27.132'
#     foreign_option_2='dhcp-option DNS 193.43.27.133'
#     foreign_option_3='dhcp-option DOMAIN be.bnc.ch'
#

if [ ! -x /sbin/resolvconf ] ; then
    logger "[OpenVPN:update-resolve-conf] missing binary /sbin/resolvconf";
    exit 0;
fi

[ "$script_type" ] || exit 0
[ "$dev" ] || exit 0

split_into_parts()
{
        part1="$1"
        part2="$2"
        part3="$3"
}

case "$script_type" in
  up)
        NMSRVRS=""
        SRCHS=""
        foreign_options=$(printf '%s\n' ${!foreign_option_*} | sort -t _ -k 3 -g)
        for optionvarname in ${foreign_options} ; do
                option="${!optionvarname}"
                echo "$option"
                split_into_parts $option
                if [ "$part1" = "dhcp-option" ] ; then
                        if [ "$part2" = "DNS" ] ; then
                                NMSRVRS="${NMSRVRS:+$NMSRVRS }$part3"
                        elif [ "$part2" = "DOMAIN" ] ; then
                                SRCHS="${SRCHS:+$SRCHS }$part3"
                        fi
                fi
        done
        R=""
        [ "$SRCHS" ] && R="search $SRCHS
"
        for NS in $NMSRVRS ; do
                R="${R}nameserver $NS
"
        done
        echo -n "$R" | /sbin/resolvconf -a "${dev}.openvpn"
        ;;
  down)
        /sbin/resolvconf -d "${dev}.openvpn"
        ;;
esac


```
{% endcode %}

* Buka file .ovpn anda kemudian tambahkan baris kode pada baris ke 18-24 pada config tersebut:

```text
client
dev tun
proto tcp
sndbuf 0
rcvbuf 0
remote xxxxxx 1194
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
auth SHA512
cipher AES-256-CBC
comp-lzo
setenv opt block-outside-dns
key-direction 1
verb 3
#block-outside-dns
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
down-pre
dhcp-option DNSSEC allow-downgrade
dhcp-option DOMAIN-ROUTE .
```

### Windows

* Pada windows, hal ini lebih simpel _\(sudah kami test pada OS Windows 7\)_. Anda hanya perlu menambahkan 1 baris code `block-outside-dns` pada config .ovpn anda. \(Cek baris ke-18\).

```text
client
dev tun
proto tcp
sndbuf 0
rcvbuf 0
remote xxxxxx 1194
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
auth SHA512
cipher AES-256-CBC
comp-lzo
setenv opt block-outside-dns
key-direction 1
verb 3
block-outside-dns
```

{% hint style="success" %}
_**Pastikan aplikasi openVPN anda versi terbaru atau diatas versi v2.3.9+ agar fitur diatas bekerja maksimal.**_
{% endhint %}

## Test VPN Setelah Di Update

* Login ke VPN seperti biasa
* Cek hasil result tentu berbeda dari logs setelah ada penambahan konfigurasi pada file .ovpn

![1.3 - Perubahan Config dan Hasil ketika login dengan OpenVPN](../../.gitbook/assets/fix-dns-leak-di-linux-ubuntu-atau-windows-1-3.png)

* Sekarang kita test kembali dengan web `dnsleaktest`

![1.4 - Hasil test setelah perubahan config](../../.gitbook/assets/fix-dns-leak-di-linux-ubuntu-atau-windows-1-4.png)

* Terlihat pada gambar 1.4 bahwa DNS yang muncul saat ini adalah 3, yaitu :
  * Telkomsel
  * Cloudflare
  * Google
* Test kembali buka website `vimeo`

![1.5 - Test Web Vimeo Dengan Config Baru](../../.gitbook/assets/fix-dns-leak-di-linux-ubuntu-atau-windows-1-5.png)

* Saat ini web vimeo sudah bisa dibuka, karena dns telah di update. Selesai

