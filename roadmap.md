---
description: List fitur atau task PFN yang akan datang atau sedang dikerjakan
---

# ROADMAP

## 2020

### Januari

* [ ] Upgrade WHMCS Addons WSGateway
  * [ ] Use Phone Number and Custom Fields
* [ ] 
## 2019

### Desember

* [ ] Migrasi Proxy Server FREE
* [ ] Migrasi Proxy Server VIP
* [ ] Upgrade Mirror Files From Manual Ticket Support to Website
  * [ ] Build Backend Scripts
  * [ ] Build Automatic Script Downloader
  * [ ] Build Frontend User

